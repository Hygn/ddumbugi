import websocket
import json
import threading
import requests
import time
from bs4 import BeautifulSoup
import RESTDCAPI
import getpass
###IMPORTSETTINGS###
import sys
if 'win' in sys.platform.lower():
    Winenv = True
else:
    Winenv = False
if Winenv: 
    try:
        from win10toast import ToastNotifier
    except ImportError:
        Winenv = False
###BEGINCONFIG###
NotificationEnable = True
DCinsideEnable = True
SanitizeOutput = True
cf_appid = '1256670'
DC_boardID = "lobotomycorporation"
###ENDCONFIG###
if DCinsideEnable:
    global dcapi
    global DC_id
    global DC_password
    DC_id = input('DCINSIDE ID: ')
    DC_password = getpass.getpass(prompt='DCINSIDE PASSWORD: ',stream=False)
    dcapi = RESTDCAPI.API()
    dcapi.login(DC_id,DC_password)
Winenv = Winenv and NotificationEnable
header = {'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.72 Safari/537.36',
          'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
          'sec-ch-ua-mobile': '?0',
          'sec-fetch-dest': 'empty',
          'sec-fetch-mode': 'cors',
          'sec-fetch-site': 'same-origin',
          'x-requested-with': 'XMLHttpRequest'}
if Winenv: toast = ToastNotifier()
def getUpdateDetail(index):
    if SanitizeOutput == False:
        return [True,'']
    data = requests.get(f'https://steamdb.info/api/GetAppHistory/?lastentry=0&appid={cf_appid}', headers=header)
    data = BeautifulSoup(data.content.decode('utf-8'),features='lxml').findAll('div',class_='panel panel-history')[index]
    if 'depots' in str(data).lower():
        if 'added' in str(data).lower():
            return [True,BeautifulSoup(str(data).replace('<span class="branch-name">','\n'),features='lxml').text]
        else:
            return [False,'']
    else:
        return [False,'']
def noti(msg):
    try:
        if Winenv: toast.show_toast("DB뜸부기",msg,duration=5)
    except:
        pass
    return
def on_message(ws, message, dcapi=dcapi, cf_appid=cf_appid, DC_boardID=DC_boardID):
    data = json.loads(message)
    try:
        appId = '0'
        try:
            appId = list(data['Apps'].keys())[0]
            appNm = data['Apps'][appId]
        except:
            pass
        if int(appId) == int(cf_appid): 
            detail = getUpdateDetail(0)
            if detail[0]:
                print(appNm+' DB뜸')
                threading.Thread(target=noti,args=("DB뜸!!\n"+appNm,)).start()
                for i in range(5):
                    i
                    print('writing...')
                    writeRes = dcapi.write('DB뜸',f'https://steamdb.info/app/{cf_appid}/history/\n(자동 작성 된 글임)\n\n\n\n{detail[1]}\n\nhttps://gitlab.com/Hygn/ddumbugi',DC_boardID)
                    #writeRes = dcapi.write('API테스트',f'https://steamdb.info/app/{cf_appid}/history/\n(자동 작성 된 글임)\n\n\n\n{detail[1]}\n\nhttps://gitlab.com/Hygn/ddumbugi',DC_boardID)
                    if writeRes == True:
                        break
                    else:
                        print('retrying...')
                        dcapi.logout()
                        dcapi = RESTDCAPI.API()
                        dcapi.login(DC_id,DC_password)
                    if i == 4:
                        print('write Failed')
                        exit()
            else:
                print(appNm+' 변경사항 발생')
        else:
            print(appNm+' 뜸부기')
    except Exception as e:
        open('ddumbugi.log','a').write(f"\n{e}")
        pass
def on_error(ws, error):
    print(error)
def on_close(ws):
    print("### DISCONNECTED ###")
    threading.Thread(target=noti,args=("DISCONNECTED",)).start()
def on_open(ws):
    print("### CONNECTED ###")
    threading.Thread(target=noti,args=("CONNECTED",)).start()
if __name__ == "__main__":
    websocket.enableTrace(True)
    ws = websocket.WebSocketApp("wss://steamdb.info/api/realtime/",
                              on_open = on_open,
                              on_message = on_message,
                              on_error = on_error,
                              on_close = on_close)
while True:
    ws.run_forever()
    print('Connection closed\npress ctrl-c to exit\nauto reconnect in 10 seconds')
    try:
        time.sleep(10)
    except KeyboardInterrupt:
        if DCinsideEnable:
            dcapi.logout()
        print('quit')
        quit()
