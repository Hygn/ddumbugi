import requests
from bs4 import BeautifulSoup
import json
import toasciiart
from analyticsGen import gaGen
class API():
    session = requests.session()
    GETheader = {'user-agent':'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Mobile Safari/537.36',
                 'referer':'https://m.dcinside.com/'}
    XHRLoginheader = {'user-agent':'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Mobile Safari/537.36',
                  'referer':'https://m.dcinside.com/',
                  'X-Requested-With': 'XMLHttpRequest',
                  'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
                  'sec-ch-ua-mobile': '?1',
                  'Sec-Fetch-Dest': 'empty',
                  'Sec-Fetch-Mode': 'cors',
                  'Sec-Fetch-Site': 'same-origin'}
    Writeheader = {'user-agent':'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Mobile Safari/537.36',
                  'X-Requested-With': 'XMLHttpRequest',
                  'sec-ch-ua': '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
                  'sec-ch-ua-mobile': '?1',
                  'Sec-Fetch-Dest': 'empty',
                  'Sec-Fetch-Mode': 'cors',
                  'Sec-Fetch-Site': 'same-origin',
                  "Accept-Encoding": "gzip, deflate, br",
                  "Accept": "*/*",
                  'Origin':'https://m.dcinside.com',
                  "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",}
    def login(self, id_, password):
        self.session.get('https://m.dcinside.com/',headers=self.GETheader)
        def __login(self, id_, password, code='' ,captcha=False):
            if captcha:
                loginform = self.session.get('https://m.dcinside.com/auth/login?r_url=https%3A%2F%2Fm.dcinside.com&mode=&bot=1',headers=self.GETheader).text
                capturl = str(BeautifulSoup(loginform,features='lxml').find('div',class_='code')).split('<img border="0" id="code_img" src="')[1].split('"')[0]
                capt = self.session.get(capturl.replace('amp;',''),headers=self.GETheader).content
                open('out.jpg','wb').write(capt)
                toasciiart.printAscii('out.jpg')
                code = input('code = ')
            else:
                loginform = self.session.get('https://m.dcinside.com/auth/login?r_url=https%3A%2F%2Fm.dcinside.com',headers=self.GETheader).text
            post_payload = {}
            fieldList = []
            for i in BeautifulSoup(loginform,features='lxml').findAll('input'):
                if 'hidden' in str(i):
                    fieldList.append(str(i))
            for q in fieldList:
                for i in q.split('='):
                    if 'name' in i:
                        _key = str(q).split('=')[str(q).split('=').index(i)+1].replace('"','').replace('/','').replace('>','').split(' ')[0]
                    if 'value' in i:
                        _data = str(q).split('=')[str(q).split('=').index(i)+1].replace('"','').replace('/','').replace('>','').split(' ')[0]
                post_payload.update({_key:_data})
            payload = {'randcode':post_payload['rand_code'],
                       'con_key':post_payload['con_key'],
                       'user_id': id_,
                       'token_verify': 'dc_login',
                       'code': code}
            con_key = self.session.post('https://m.dcinside.com/ajax/access',data=payload,headers=self.XHRLoginheader).text
            con_key = json.loads(con_key)['Block_key']
            post_payload.update({'user_id': id_,
                                 'user_pw': password,
                                 'captcha_code':code,
                                 'r_url': 'https://m.dcinside.com',
                                 'id_chk': 'on',
                                 'con_key': con_key})
            data = self.session.post('https://dcid.dcinside.com/join/mobile_login_ok_new.php',data=post_payload,headers=self.XHRLoginheader).text
            #print(data)
            if 'bot=1' in data:
                return {'captcha':True,'failLogin':False}
            elif 'rucode=1' in data:
                return {'captcha':True,'failLogin':False}
            else:
                return {'captcha':False,'failLogin':False}
        for i in range(10):
            initalLogin = __login(self, id_,password,'',captcha=False)
            if initalLogin == {'captcha':True,'failLogin':False}:
                print('captcha datected')
                loginStatus = __login(self, id_,password,'',captcha=True)
                if loginStatus == {'captcha':False,'failLogin':False}:
                    print('login successful')
                    break
                elif loginStatus == {'captcha':False,'failLogin':True}:
                    print('id or password is incorrect')
                    exit()
            elif initalLogin == {'captcha':False,'failLogin':False}:
                print('login successful')
                break
            elif initalLogin == {'captcha':False,'failLogin':True}:
                print('id or password is incorrect')
                exit()
            else:
                raise Exception
            if i == 9:
                print('loginFailed')
                exit()
    def write(self,subject,content,board_id):
        WriteXHRheader = self.Writeheader
        url = 'https://m.dcinside.com/write/'+board_id
        writeform = self.session.get(url,headers=self.GETheader).text
        csrf = writeform.split('meta name="csrf-token" content="')[1].split('"')[0]
        WriteXHRheader.update({'X-CSRF-TOKEN': csrf,
                               'Referer': url,})
        post_payload = {}
        fieldList = []
        for i in BeautifulSoup(writeform.split('<form id="ImgFree" name="ImgFree" method="POST" style="display: none;" enctype="multipart/form-data">')[0],features='lxml').findAll('input'):
            if 'hid' in str(i):
                fieldList.append(str(i))
        for q in fieldList:
            for i in q.split('='):
                if 'name' in i:
                    _key = str(q).split('=')[str(q).split('=').index(i)+1].replace('"','').replace('/','').replace('>','').split(' ')[0]
                if 'value' in i:
                    _data = str(q).split('=')[str(q).split('=').index(i)+1].replace('"','').replace('/','').replace('>','').split(' ')[0]
            post_payload.update({_key:_data})
        for i in post_payload.keys():
            if 'honey' in i:
                honey = i 
        writejsID = writeform.split('js/write.js?v=')[1].split('"')[0]
        uniq_key = self.session.get('https://m.dcinside.com/js/write.js?v='+writejsID,headers=self.GETheader).text.split('$(\'<input name="')[1].split('"')[0]
        board_name = str(BeautifulSoup(writeform,features="lxml").find('a',class_='gall-tit-lnk').text)
        customcookie = {
                "m_dcinside_" + board_id: board_id,
                "m_dcinside_lately": requests.utils.requote_uri(board_id + "|" + board_name + " $minor$,"),
                "_ga": gaGen(1,'dcinside.com')}
        con_key = self.session.post('https://m.dcinside.com/ajax/access',data={'token_verify': 'dc_check2'},headers=WriteXHRheader,cookies =customcookie)
        con_key = json.loads(con_key.text)['Block_key']
        payload = {'subject': subject,
                   'memo': content,
                   'id': board_id,
                   'mode': 'write',
                   'is_mini':'' }
        self.session.post('https://m.dcinside.com/ajax/w_filter',data=payload,headers=WriteXHRheader).text
        post_payload.update({
            'subject':  subject,
            'memo': content,
            'Block_key':con_key,
            uniq_key:honey,
            'imgSize':'850'
        })
        data = self.session.post('https://mupload.dcinside.com/write_new.php',data=post_payload,headers=WriteXHRheader).text
        if '<meta http-equiv="refresh" content="0;' not in data:
            data = data.replace('\xa0','')
            open('ddumbugi.log','a',encoding='utf-8').write(f"\n{data}")
            return False
        else:
            return True
    def logout(self):
        self.session.get('https://m.dcinside.com/auth/logout?r_url=https%3A%2F%2Fm.dcinside.com')