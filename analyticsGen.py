def gaGen(ga_ver,domain,offset=None):
    import datetime
    import random
    ga_ver = str(int(ga_ver))
    if offset == None:
        timestamp = str(int(datetime.datetime.now().timestamp()) + random.randint(-100000,100000))
    else:
        timestamp = str(int(datetime.datetime.now().timestamp()) + offset)
    cl_id = str(random.randint(100000000,999999999))
    components = str(len(domain.split('.')))
    _ga = f"{ga_ver}.{components}.{cl_id}.{timestamp}"
    return _ga
